# Turbine_Project : Représentation des Diagnostics de performance énergétique (DPE) sur l’Agglomération grenobloise

## Descriptif de contenu des fichiers

* le dossier documentation contient les fichiers expliquant la démarche suivie, le cahier de charge, les sources utilisées et la présentation finale
* .gitlab-ci.yml contient la configuration pour le déploiement des résultats
* cartographie_dpe_ges.ipynb est le Jupyter Notebook qui contient les deux cartographies, DPE et GES pour déploiement
* les autres Jupyter Notebooks contiennent les cartographies séparément
## Technologies utilisées
* Python, sqlite3, Jupyter Notebook, Git, GitLab

## Lien vers le déploiment des deux cartographies, DPE et GES
https://malikaouassou.gitlab.io/turbine_project


