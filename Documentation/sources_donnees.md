## Jeu de données des communes de la métropole
https://data.metropolegrenoble.fr/ckan/dataset/les-communes-de-la-metropole

## Contours IRIS
https://geoservices.ign.fr/documentation/diffusion/telechargement-donnees-libres.html#irisge

## Données DPE de la France entière
https://data.ademe.fr/datasets/dpe-france

## Données DPE de l'Isère
https://data.ademe.fr/datasets/dpe-38

## Nombre des logements en 2017
https://www.data.gouv.fr/fr/datasets/logement/


